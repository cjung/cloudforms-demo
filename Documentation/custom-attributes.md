# Custom Attributes

CloudForms does not aim to replace a CMDB solution, however we can use custom attributes to add additional data and associate it to a given object. Custom attributes are shown in the UI if defined. They can also be utilized in Reports.

There is no out of the box functionality to create, edit and delete custom attributes from the Web UI. Due to its easy to extend nature, we can add those missing buttons ourselves.

Navigate to a Virtual Machine and note the new menu "CMDB" with three menus:

- Create Custom Attribute
- Delete Custom Attribute
- Update Custom Attribute

Each button is using a custom dialog and Automate code to perform the respective action. All methods can be found in the [Automate](../Automate) sub folder in the [CustomAttributes](../Automate/Integration/CustomAttributes/) class.

:heavy_check_mark: ***NOTE*** After using one of the above buttons, keep in mind that Automate code runs in the background. You will have to use the reload icon to refresh the screen to show the results of the previous action.

More Details can also be found in the blog post [CRUD for Custom Attributes in CloudForms or ManageIQ](http://www.jung-christian.de/post/2017/12/custom-attributes/).